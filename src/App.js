import React, { useEffect, useState, Fragment } from "react";
import "./App.css";
import MainApp from "./main/MainApp";
import TestDetailsContext from "./TestDetailsContext";

function App() {
  const [testEntries, setTestEntries] = useState(null);
  const [appReady, setAppReady] = useState(false);
  let content;
  const onReady = data => {
    setTestEntries(data);
    setAppReady(true);
  };
  useEffect(() => {
    console.log("mounted");
    fetch("/data/sample.json")
      .then(response => response.json())
      .then(data => onReady(data));
  }, []);
  //Render
  const contextData = { testEntries };
  if (!appReady) {
    content = <h2> Loading..</h2>;
  } else {
    content = (
      <Fragment>
        <h2>Loaded</h2>
        <p>Data is</p>
        <TestDetailsContext.Provider value={contextData}>
          <MainApp />
        </TestDetailsContext.Provider>
      </Fragment>
    );
  }
  return <div className="App">{content}</div>;
}

export default App;
