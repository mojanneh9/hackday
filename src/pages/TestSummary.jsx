import React, { Fragment } from "react";
import TestDetailsContext from "../TestDetailsContext";
import TestSummaryRows from "./TestSummaryRows";
function TestSummary() {
  return (
    <TestDetailsContext.Consumer>
      {contextData => (
        <Fragment>
          <div className="heading">
            <h1>Test results</h1>
            <button onClick="location.href='./allTests.html'" type="button">
              See all test details
            </button>
          </div>

          <div id="testResults" className="tabcontent active">
            <table>
              <thead>
                {/* Test case name - {testEntries[index].name}*/}
                <th>Test name</th>
                {/* Number of tests in the suite where all steps executed successfully */}
                <th>Passed</th>
                {/* Number of tests in the suite where there are any step execution failures */}
                <th>Failed</th>
                <th></th>
              </thead>
              <TestSummaryRows rows={contextData.testEntries} />
            </table>
          </div>
          <pre>{JSON.stringify(contextData.testEntries, null, 2)}</pre>
        </Fragment>
      )}
    </TestDetailsContext.Consumer>
  );
}

export default TestSummary;
