import React from "react";

const TestSummaryRows = props => {
  console.log(props.rows.testEntries);
  const rows = props.rows.testEntries.map(row => {
    const rowTitle = `${row.name} - ${row.description}`;
    let passes = 0;
    let fails = 0;
    row.steps.forEach(step => {
      const subDesc = step.subDesc;
      subDesc.forEach(test => {
        const status = test.status;
        if (status === "Pass") {
          passes++;
        } else {
          fails++;
        }
      });
    });
    return (
      <tr key={rowTitle}>
        <td>{rowTitle}</td>
        <td>{passes}</td>
        <td>{fails}</td>
        <td>
          <a href="./test.html">Show more</a>
        </td>
      </tr>
    );
  });
  console.log(rows);

  return (
    <>
      <tr>{rows}</tr>
    </>
  );
};
export default TestSummaryRows;
