import React from "react";
import PropTypes from "prop-types";
import TestDetailsContext from "../TestDetailsContext";

function GraphSummary() {
  return (
    <TestDetailsContext.Consumer>
      {contextData => (
        <>
          <h2> Graph Summary</h2>
          <p>Graphy goes here.....</p>
          {console.log(contextData)}

          <pre>{JSON.stringify(contextData.testEntries, null, 2)}</pre>
        </>
      )}
    </TestDetailsContext.Consumer>
  );
}
GraphSummary.propTypes = {
  testEntries: PropTypes.object.isRequired
};
export default GraphSummary;
