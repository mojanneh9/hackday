import React, { useState } from "react";
import TestSummary from "../pages/TestSummary";
import GraphSummary from "../pages/GraphSummary";

const TEST_SUMMARY = "testSummary";
const GRAPH_SUMMARY = "graphSummary";

function MainApp() {
  const [page, setPage] = useState(TEST_SUMMARY);
  const onPageChange = page => {
    setPage(page);
  };
  return (
    <div className="container">
      <div className="tab">
        <button
          disabled={page === TEST_SUMMARY}
          className="tablinks"
          onClick={() => onPageChange(TEST_SUMMARY)}
        >
          Test summary
        </button>
        <button
          disabled={page === GRAPH_SUMMARY}
          className="tablinks"
          onClick={() => onPageChange(GRAPH_SUMMARY)}
        >
          Graph summary
        </button>
      </div>
      {page === TEST_SUMMARY && <TestSummary />}
      {page === GRAPH_SUMMARY && <GraphSummary />}
    </div>
  );
}

export default MainApp;
